
import sys

def dump(value):
    print value, "=>", dir(value)
    
# dump(0)
# dump(1.0)
# dump(0.0j) # complex number
# dump([]) # list
# dump({}) # dictionary
# dump("string")
# dump(len) # function
# dump(sys) # module

print dir("")

print "abbfafbgfahrthre".count("a")